/*String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};*/

(function ($) {

    // najdeme si vsetky galerie
    var galleries = $('.gallery-set');

    // skryjeme vsetky 
    galleries.hide();

    // v menu najdeme link, ktory je selected
    var selected = $('.menu').find('.selected');
  

    // vytvorim funkciu show gallery, ktora akceptuje selectnuty prvok menu na zaklade ktoreho vyberieme a zobrazime galeriu
    function showGallery(selected) {

        // ak takyto element existuje, najdeme z jeho linku idcko elementu
        // toto idcko pouzijeme na najdeme galerie, ktoru chceme zobrazit
        if (selected.length) {
            var id = selected.find('a').attr('href');
            selectedGallery = $(id);
        }

        // ak tato galeria existuje bude to nasa newGallery, inak new Gallery bude prva galeria v zozname
        var newGallery = selectedGallery.length ? selectedGallery : galleries.eq(0);

        // vsetky galerie ktore nie su nasa nova galeria schovame
        galleries.not(newGallery).hide();

        // zobrazime nasu novu galeriu
        newGallery.show().addClass(selected.data('class') || 'fadeInLeft');
    }

    // hned ju zavolame
    showGallery(selected);

    // ak klikneme na link v menu, nastavime mu class selected a vsetky surodencom ho odoberieme
    // aby vzdy bol selected iba jeden
    // nasledne zobrazime aktualnu galeriu
    
    $('.menu li').on('click', function (event) {
        var fadeClass = 'fadeIn' + $(this).data('from')/*.capitalize();*/
        

        $(this)
            .data('class', fadeClass)
        //    .addClass('selected')
          //  .siblings().removeClass('selected');

        showGallery($(this));
        event.preventDefault();
    });


    var list = $('.info');
    list.find("dd").hide();
    list.find("dt").on("click", function (e) {
        $(this).next().slideToggle();
        e.preventDefault();
    });

    var cover = $("#cover"),
        covers = $(".fadecovers");

    covers.children(":not(:last)").hide();
    var sliderInterval = setInterval(function () {
        covers.children(":last")
            .fadeOut(2000, function () {
                $(this).prependTo(covers);
      })
            .prev().fadeIn(2000);
    }, 2000);





    //scroll opacity

    var headerBg = document.getElementById('bg')
    window.addEventListener('scroll', function () {
        headerBg.style.opacity = 1 - +window.pageYOffset / 1000 + ''
        headerBg.style.top = +window.pageYOffset + 'px'
        headerBg.style.backgroundPositionY = - +window.pageYOffset / 2 + 'px'
    })



    var info = $('.info'),
        position1 = $('#free__position');
    info.hide();
    position1.on('click', function () {
        info.fadeIn();


    })

    //
     var html = $('html');
     var menuOpenClass = 'menu-opened';
     var menuBtn = $('.menu__btn');

     menuBtn.on('click', function () {
         if (html.hasClass(menuOpenClass)) {
             closeMenu();
         } else {
             openMenu();
         }
     });


     $(document).on('click', 'a[href^="#"]', function (e) {
         e.preventDefault();

         var target = $(this).attr('href');
         var sectionOffsetFromTop = $(target).offset().top ;

         closeMenu();

         $('html, body').stop().animate({
             scrollTop: sectionOffsetFromTop
         }, 500);
     });

     function openMenu() {
         html.addClass(menuOpenClass);
     }

     function closeMenu() {
         html.removeClass(menuOpenClass);
     }

    

    var lastScrollTop = 0;
        navbar = document.getElementById("header");
        window.addEventListener("scroll", function(){
            scrollTop = window.pageYOffset || document
            .documentElement.scrollTop;
            if (scrollTop > lastScrollTop){
                header.style.top="-62px";
            } else { navbar.style.top="0";

            }
            lastScrollTop = scrollTop;
        });




    
    $(function(){
        $("#num").counterUp({
            delay:10,
            time:1000
        })
         $("#num1").counterUp({
            delay:10,
            time:1000
        })
         $("#num2").counterUp({
            delay:10,
            time:1000
        })
         $("#num3").counterUp({
            delay:10,
            time:1000
        })
    })

})(jQuery);